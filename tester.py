import requests
import json
import shutil

if __name__ == "__main__":
    # execute only if run as a script
    json_req = requests.get("https://api.openligadb.de/getbltable/bl2/2021").json()

    print(json_req)

    with open("2021.json", "w") as json_file:
        json.dump(json_req, json_file)
        json_file.close()

source = r"2021.json"
destination = r"C:\users\TRE\Desktop"

shutil.move(source,destination)


