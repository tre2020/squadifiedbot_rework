"""
Import Essentials
"""
import sys
from cogs.maphistory.models.mapmessages import Mapmessages
from discord import *
from discord.ext import commands
from settings import *
from utils import *
from database import setup_database

info(f"Starting main.py: Debug Mode: {DEBUG}")
# Before starting check if we have everything we need to run the bot
if not check_requirements():
    error("Gracefully stopping. Not all required environment variables set! Check your config!")
    sys.exit(0)


# Setup Database
setup_database()

# Setup Bot
client_bot = commands.Bot(command_prefix=".")


@client_bot.event
async def on_ready():
    error("[S] SQUADIFIED DISCORD BOT")
    error("VERSION 1.10")
    error("LOADING EXTENSIONS")
    error("COPYRIGHT [S] SERGEANT STONE - 2021")


# Load extensions
initial_extensions = ['cogs.maphistory.maphistory',
                      'cogs.botstatus.status',
                      #'cogs.cleaner.cleaner',
                      'cogs.ticketsystem.ticketsystem',
                      #'cogs.seedingreaction.seedingreaction',
                      'cogs.whitelist.whitelist',
                      #'cogs.rcon.rcon',
                      ]
for extension in initial_extensions:
    client_bot.load_extension(extension)


# Run the bot
client_bot.run(DISCORD_TOKEN)
