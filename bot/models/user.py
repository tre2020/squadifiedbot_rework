#################################################################
#################################################################
# NEUES MODEL - ERSETZT WHITELISTREQUEST.PY
#################################################################
#################################################################
#################################################################
# WORK IN PROGRESS
#################################################################
#################################################################
#################################################################


from peewee import *
from settings import db
import datetime


class User(Model):
    discord_name = CharField()
    steam_name = CharField(null=True)
    vac_bans = IntegerField(null=True)
    days_since_vac_ban = IntegerField(null=True)
    steam_id = IntegerField(null=True)
    discord_user_id = IntegerField()
    play_time = IntegerField(null=True)
    application_message = IntegerField(null=True)
    battlemetrics_profile = CharField(null=True)
    application_status = CharField(null=True)  # WERT STATT STRING - VERKNÜPFUNG? -> UMBAUEN!

    createdAt = DateTimeField(default=datetime.datetime.now)
    updatedAt = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db


    def save(self, *args, **kwargs):
        self.updatedAt = datetime.datetime.now()
        return super(User, self).save(*args, **kwargs)

