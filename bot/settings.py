import json
import logging
import logging.config
import os
import pathlib
import yaml
import sys
from datetime import datetime
from peewee import *
from dotenv import load_dotenv

# load .env file
load_dotenv()

# set base directory
BASE_DIR = pathlib.Path(__file__).parent
DB_PATH = BASE_DIR / 'data' / 'squadifiedbot.db'
# set Sqlite database
db = SqliteDatabase(str(DB_PATH.resolve()))

# Trigger DEBUG for Development or not
DEBUG = os.getenv("DEBUG", "False") == "True"

# Discord
# Discord Tokens
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")
OWNER = int(os.getenv("OWNER", 0))
# Discord Channels
MOD_CHAT_ID = int(os.getenv("MOD_CHAT_ID"))
WHITELIST_EDIT_CHANNEL = int(os.getenv("WHITELIST_EDIT_CHANNEL"))

# Discord Categories
SUPPORT_CAT = int(os.getenv("SUPPORT_CAT"))
TICKET_CAT = int(os.getenv("TICKET_CAT"))
WHITELIST_APPLICATION_CATEGORY = int(os.getenv("WHITELIST_APPLICATION_CATEGORY"))

# Discord Roles
SERVER_ADMIN_ROLE = int(os.getenv("SERVER_ADMIN_ROLE"))
COMLEAD_ROLE = int(os.getenv("COMLEAD_ROLE"))
VICELEAD_ROLE = int(os.getenv("VICELEAD_ROLE"))
TECH_ROLE = int(os.getenv("TECH_ROLE"))


# Discord Seeding
SEEDING_ROLE = int(os.getenv("SEEDING_ROLE"))
SEEDING_REACTION_CHANNEL = int(os.getenv("SEEDING_REACTION_CHANNEL"))
SEEDING_REACTION_MESSAGE = int(os.getenv("SEEDING_REACTION_MESSAGE"))
SEEDING_CHAT_CHANNEL = int(os.getenv("SEEDING_CHAT_CHANNEL"))


# 3rd Party Integrations
# Battle Metrics
BM_TOKEN = os.getenv("BM_TOKEN")
BM_SERVER_ID_1 = os.getenv("BM_SERVER_ID_1")
# Steam Integration
STEAM_TOKEN = os.getenv("STEAM_TOKEN")

# Setup logging
with open(BASE_DIR / "logging.yml", 'r') as stream:
    try:
        logConfig = yaml.safe_load(stream.read())

        logging.config.dictConfig(logConfig)
    except Exception as e:
        print(e)
        print("Could not load logging.yml")
        sys.exit(1)
