
from enum import Enum
from settings import *

_logger = logging.getLogger("file")


class LogLevel(Enum):
    """Defines a log level, only used in debug mode for log methods

    Args:
        Enum (int): Describes what log level it has
    """
    DEBUG = 0
    INFO = 1
    WARN = 2
    ERROR = 3
    FATAL = 4


class bcolors:
    """Only works on Linux terminal! 
    Colors the output of the terminal message 
    always use ENDC at the end to reset
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def log(msg, log_type=LogLevel.INFO):
    """A universal Loggin method that switches based on DEBUG mode
    In DEBUG it prints
    In Production it logs to file
    Try to use debug, info, warn, error, fatal instead of directly log
    Args:
        msg str: Message to log
        log_type (LogLevel, optional): Describe severity. Defaults to LogLevel.INFO.
    """
    if DEBUG is True:
        # defualt for INFO
        prefix = f"{bcolors.OKBLUE}[#]"
        if log_type == LogLevel.DEBUG:
            prefix = f"[+]"
        if log_type == LogLevel.WARN:
            prefix = f"{bcolors.WARNING}[!]"
        if log_type == LogLevel.ERROR:
            prefix = f"{bcolors.FAIL}[@]"
        if log_type == LogLevel.FATAL:
            prefix = f"{bcolors.FAIL}[~]"
        print(f"{prefix} {msg} {bcolors.ENDC}")
    # else:
    #     # use normal logging method
    #     if log_type == LogLevel.DEBUG:
    #         _logger.debug(msg)
    #     if log_type == LogLevel.INFO:
    #         _logger.info(msg)
    #     if log_type == LogLevel.WARN:
    #         _logger.warning(msg)
    #     if log_type == LogLevel.ERROR:
    #         _logger.error(msg)
    #     if log_type == LogLevel.FATAL:
    #         _logger.critical(msg)


def debug(msg):
    log(msg, LogLevel.DEBUG)


def info(msg):
    log(msg, LogLevel.INFO)


def warn(msg):
    log(msg, LogLevel.WARN)


def error(msg):
    log(msg, LogLevel.ERROR)


def fatal(msg):
    log(msg, LogLevel.FATAL)


def check_requirements():
    """Checks if all the environment variables are set

    Returns:
        bool: True if everything is fine, otherwise False
    """
    if not DISCORD_TOKEN:
        return False
    if not OWNER:
        return False
    if not MOD_CHAT_ID:
        return False
    if not WHITELIST_EDIT_CHANNEL:
        return False
    if not SUPPORT_CAT:
        return False
    if not TICKET_CAT:
        return False
    if not WHITELIST_APPLICATION_CATEGORY:
        return False
    if not SERVER_ADMIN_ROLE:
        return False
    if not COMLEAD_ROLE:
        return False
    if not VICELEAD_ROLE:
        return False
    if not TECH_ROLE:
        return False
    if not SEEDING_ROLE:
        return False
    if not SEEDING_REACTION_CHANNEL:
        return False
    if not SEEDING_REACTION_MESSAGE:
        return False
    if not SEEDING_CHAT_CHANNEL:
        return False
    if not BM_TOKEN:
        return False
    if not BM_SERVER_ID_1:
        return False
    if not STEAM_TOKEN:
        return False
    return True
