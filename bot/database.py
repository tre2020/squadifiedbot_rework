from models.user import User
from cogs.maphistory.models.map import Map
from cogs.maphistory.models.mapmessages import Mapmessages
from cogs.whitelist.models.whitelistconfig import Whitelistconfig
from cogs.whitelist.models.whitelistrequest import Whitelistrequest
from cogs.ticketsystem.models.ticketdata import Ticketdata
from cogs.ticketsystem.models.ticketutils import Ticketutils
from settings import db
from utils import log


def setup_database():
    """Connects to database and creates models
    """
    try:
        db.connect()
        db.create_tables([Map, Mapmessages, Whitelistconfig,
                          User, Ticketdata, Ticketutils])
    except Exception as e:
        log("Could not setup database...")
