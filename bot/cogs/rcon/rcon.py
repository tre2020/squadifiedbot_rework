#################################################################################################
# Import Essentials 
#################################################################################################

import json
import discord
import asyncio
import steam
import time
from discord.ext import commands
from settings import *
from utils import *

from valve.rcon import RCON



class Rcon(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    async def broad(self, ctx):


        warn("Started Connection")
        SERVER_ADDRESS = ("37.72.145.241", 21114)
        PASSWORD = "testrange"
        map = ctx.message.content

        with RCON(SERVER_ADDRESS, PASSWORD) as rcon:
            print(rcon(f"AdminBroadcast {map}"))
            warn("Send RCON MESSAGE")
            return

    

def setup(bot):
    bot.add_cog(Rcon(bot))