#################################################################################################
# Import Essentials 
#################################################################################################

import json
from settings import SEEDING_CHAT_CHANNEL, SEEDING_REACTION_MESSAGE, SEEDING_ROLE
import discord
import asyncio
from discord import Member
from discord import guild
from discord import reaction
from discord import utils
from discord import channel
from discord.ext import commands
from settings import *
from utils import *


class Roles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


#################################################################################################
# Funktionen
#################################################################################################

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):

        
        guild = self.bot.get_guild(payload.guild_id)
        seeding_chat_channel = self.bot.get_channel(SEEDING_CHAT_CHANNEL)
        user = (payload.member)
        seedingrole = guild.get_role(SEEDING_ROLE)
        seedingmessage = SEEDING_REACTION_MESSAGE

        if payload.message_id == seedingmessage and payload.emoji.name == "✅":
                
                await user.add_roles(seedingrole)
                msg = await seeding_chat_channel.send("Willkommen im Kreise der Seeder {}! Vielen Dank für deine Unterstützung!".format(user.mention))
                await msg.add_reaction("️️️:squadified:750087986359566517")
                info("Seedingrole Log: Added user to Seedingchannel.")



    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        guild = self.bot.get_guild(payload.guild_id)
        user = await guild.fetch_member(payload.user_id)
        seedingrole = guild.get_role(SEEDING_ROLE)
        seedingmessage = SEEDING_REACTION_MESSAGE


        if payload.message_id == seedingmessage and payload.emoji.name == "✅":
            await user.remove_roles(seedingrole)
            info("Seedingrole Log: Deleted user from Seedingchannel.")






def setup(bot):
    bot.add_cog(Roles(bot))
