#################################################################################################
# Import Essentials 
#################################################################################################

import json
import discord
import asyncio
from discord import Member
from discord import Guild
from discord import Reaction
from discord import utils
from discord import channel
from discord import member
from discord import Message
from discord.ext import commands
from cogs.ticketsystem.models.ticketdata import Ticketdata
from cogs.ticketsystem.models.ticketutils import Ticketutils
from utils import *
from settings import *


class Ticket(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

#################################################################################################
# Funktionen
#################################################################################################

    @commands.command()
    async def createsupport(self, ctx):

            guild = ctx.message.guild
            supportcategory = discord.utils.get(guild.categories, id=SUPPORT_CAT)
            owner = self.bot.get_user(OWNER)
            author = self.bot.get_user(ctx.author.id)

            if author == owner:

                supportchannel = await guild.create_text_channel("create-ticket", category=supportcategory)
            
                embed = discord.Embed(title="Squadified Discord Support", description="Hast du irgendwelche Fragen oder Probleme?\r"
                                    "Um ein Ticket zu öffnen, einfach auf diese Nachricht reagieren :envelope_with_arrow: \r"
                                    "Dein Ticket wird in kürze von uns bearbeitet. \r \r"
                                    "Do you have a question or a problem? \r"
                                    "To create  ticket, simply use react to this message :envelope_with_arrow: \r"
                                    "Your ticket will be addressed soon. \r", color=0xffd017, inline=True)
            
                embed.add_field(name="\r Sprachen/Languages", value=":flag_de: :flag_gb:\r"
                                                                    "\r*Dissatisfied with Squadified?*\r"
                                                                    "*Report Violations of Squad Game Server Administration Guidelines to Offworld Industries"
                                                                    " Email licensing@offworldindustries.com with the following details:*\r"
                                                                    "\r*1  Name of the server*\r"
                                                                    "*2  Any evidence such as video, pictures, etc*\r"
                                                                    "*3  Date and time of the incident*\r"
                                                                    "\r*Alternatively you can fill out the form here: https://forms.gle/R3D434WVuaY9obtT6*", inline=True)
                msg = await supportchannel.send(embed=embed)
                await msg.add_reaction("📩")

                supportchannel_record = Ticketutils()
                supportchannel_record.ticket_react_msg = msg.id
                supportchannel_record.supportchannel = supportchannel.id
                supportchannel_record.guild_id = guild.id
                supportchannel_record.save()

                info("Ticketsystem Module Log: Ticketsystem created, database up to date.")         
                info("Ticketsystem Module Log: Owner created Ticketsystem for Guild:")
                info(guild)

            elif owner != author:
                warn("Ticketsystem Module Log: WARNING: NON-OWNER TRIED TO CREATE TICKET SYSTEM!")



    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        guild = self.bot.get_guild(payload.guild_id)
        user = (payload.member)
        ticketcategory = discord.utils.get(guild.categories, id=TICKET_CAT)
        serveradmin = guild.get_role(SERVER_ADMIN_ROLE)
        comlead = guild.get_role(COMLEAD_ROLE)
        vicelead = guild.get_role(VICELEAD_ROLE)
        techniker = guild.get_role(TECH_ROLE)



#################################################################################################
# Ticket eröffnen
#################################################################################################

        ticket_utils = Ticketutils.select().where(Ticketutils.guild_id == guild.id)

        for item in ticket_utils:
            msg = item.ticket_react_msg

            if msg == payload.message_id and payload.emoji.name == "📩" and not user.bot:

                supportchannel = self.bot.get_channel(payload.channel_id)
                supportmessage = await supportchannel.fetch_message(payload.message_id)

                await supportmessage.remove_reaction("📩", user)

            
                item.ticketcount += 1
                item.opentickets += 1
                item.save()
      
                overwrites = {
                    guild.default_role: discord.PermissionOverwrite(read_messages=False),
                    user: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    serveradmin: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    comlead: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    vicelead: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    techniker: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True)
                    }

                ticketchannel = await guild.create_text_channel("{}-{}".format(user, item.ticketcount), overwrites=overwrites, topic = "SQUADIFIED TICKET SUPPORT", category = ticketcategory)
                msg = await ticketchannel.send("Hallo & Willkommen im Ticket Support der Squadified Community {}.\r \n"
                                    ":flag_de: \r"
                                    "Bitte erkläre dein Anliegen in einem verständlichen Satz. Unsere Admins werden dir helfen.\r"
                                    "Bei Fragen zu einem Bann bitte die UID und deine Steam64 ID angeben!\r"
                                    "Drücke auf den :lock: Button unterhalb, um das Ticket zu schließen.\r \n"
                                    ":flag_gb: \r"
                                    "Please explain your request in an understandable sentence. Our Admins will help you. \r"
                                    "With regards to Bans please have the respective UID and your Steam64 ID at hand! \r"
                                    "Press the :lock: below to close this Ticket.\r \n"
                                    "Deine Position in der Ticketqueue / Your position in the Ticketqueue: **{}**\r"
                                    "TicketID: {}".format(user.mention, item.opentickets, ticketchannel.id))
     
                await msg.add_reaction("🔒")

                ticket_data = Ticketdata()
                ticket_data.ticketid = ticketchannel.id
                ticket_data.userid = user.id
                ticket_data.ticketowner = user
                ticket_data.status = "Offen"
                ticket_data.save()
                



                info("Ticketsystem Module Log: New Ticket created by {}".format(user.name))
                info("Ticketsystem Module Log: Ticketcount: {}".format(item.ticketcount))
                info("Ticketsystem Module Log: Open Tickets: {}".format(item.opentickets))
                info("Ticketsystem Module Log: Ticketchannel: {}".format(ticketchannel))
                info("Ticketsystem Module Log: TicketID: {}".format(ticketchannel.id))


#################################################################################################
# Ticket schließen
#################################################################################################

   
        if not user.bot and payload.emoji.name == "🔒":

            ticket_data = Ticketdata.select().where(Ticketdata.ticketid == payload.channel_id)
            ticket_utils = Ticketutils.select().where(Ticketutils.guild_id == payload.guild_id)
                        
            for ticket in ticket_data:
                ticketid = ticket.ticketid
                userid = ticket.userid

                username = await self.bot.fetch_user(userid)

                if ticket.status == "Geschlossen":
                    return

                if ticketid == payload.channel_id:

                    ticketchannel = self.bot.get_channel(payload.channel_id)
                    await ticketchannel.send("```Ticket geschlossen von {}```".format(user))

                    for o in ticket_utils:
                        o.opentickets -= 1
                        o.save()

                    ticket.status = "Geschlossen"
                    ticket.save()

                    overwrites = {
                    guild.default_role: discord.PermissionOverwrite(read_messages=False),
                    serveradmin: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    comlead: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    vicelead: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True),
                    techniker: discord.PermissionOverwrite(read_messages=True, attach_files=True, send_messages=True, add_reactions=True)
                    }

                    await ticketchannel.edit(name="closed-{}".format(username), overwrites=overwrites)
              
                    

                    info("Ticketsystem Module Log: Ticket {} closed by {}".format(ticketid, user.name))
                    info("Ticketsystem Module Log: Open Tickets: {}".format(o.opentickets))
                    info("Ticketsystem Module Log: Ticketchannel: {}".format(ticketchannel))
                    info("Ticketsystem Module Log: Ticketchannel ID: {}".format(ticketchannel.id))
                    info("Ticketsystem Module Log: No Errors while closing ticket!")


                    
                    break


                
                




        



def setup(bot):
    bot.add_cog(Ticket(bot))
