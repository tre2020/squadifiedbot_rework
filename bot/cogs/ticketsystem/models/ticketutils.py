from peewee import *
from settings import db

class Ticketutils(Model):
    opentickets = IntegerField(null=True, default=0)
    ticketcount = IntegerField(null=True, default=0)
    ticket_react_msg = IntegerField(null=True)
    supportchannel = IntegerField(null=True)
    guild_id = IntegerField(null=True)

    class Meta:
        database = db 