import json
import discord
import os
from discord.channel import StoreChannel
import requests
import datetime
import steam
from discord import *
from discord import Message
from discord.ext import commands
from peewee import *
from requests.api import head
from utils import *
from settings import *
from steam import steamid
from cogs.whitelist.models.whitelistconfig import Whitelistconfig
from models.user import User



#adminscfg = "./squad/SquadGame/ServerConfig/Admins.cfg"
adminscfg = BASE_DIR / "cogs" / "whitelist" / "Admins.cfg"

class Whitelist(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        info("Whitelist Module Log: Module loaded.")

#################################################################################################
# reserved-slot Channel einrichten
#################################################################################################


    @commands.command()
    async def createreserved(self, ctx):
        
        bot_owner = self.bot.get_user(OWNER)
        author = self.bot.get_user(ctx.author.id)
        guild = ctx.message.guild
        reserved_category = discord.utils.get(guild.categories, id=WHITELIST_APPLICATION_CATEGORY)
        

        if author == bot_owner:

            reservedchannel = await guild.create_text_channel("reserved-slot", category=reserved_category)

            embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM", description=":flag_de: \rIn diesem Kanal können reserved-slots für den SQUADIFIED Publicserver beantragt werden.")
            embed.add_field(name="Anforderungen für einen reserved-slot:", value="-> mindestens 25 Spielstunden auf unserem Server\r-> keine schwerwiegenden Spielverstöße\r->  vernünftiges Verhalten", inline=False)
            embed.add_field(name="Was passiert nachdem ich einen Antrag gestellt habe?", value="Nachdem dein Profil auf unsere Kriterien geprüft wurde erhältst du umgehend eine Benachrichtigung.", inline=False)
            embed.add_field(name="Fragen?", value="Nutze unseren Support via #create-ticket !", inline=False)
            embed.add_field(name="Anleitung:", value="Um einen reserved-slot zu beantragen einfach in diesem Channel die URL zu deinem Steam-Profil posten, z.B: \rhttps://steamcommunity.com/id/sgtstone12/", inline=False)

            embed2=discord.Embed(description=":flag_gb: \rIn this channel, reserved-slots for the SQUADIFIED public server can be requested.")
            embed2.add_field(name="Requirements for a reserved-slot:", value="-> at least 25 hours playtime on our server\r-> no serious violations of the game\r-> good conduct", inline=False)
            embed2.add_field(name="What happens after I submit a request?", value="After your profile has been checked for our criteria you will receive a notification immediately.", inline=False)
            embed2.add_field(name="Questions?", value="Use our support via #create-ticket !", inline=False)
            embed2.add_field(name="Instructions:", value="To request a reserved-slot just post the URL to your Steam profile in this channel, e.g: \rhttps://steamcommunity.com/id/sgtstone12/", inline=False)

            await reservedchannel.send(embed=embed)
            await reservedchannel.send(embed=embed2)
                
            reservedchannel_record = Whitelistconfig()
            reservedchannel_record.whitelist_application_channel = reservedchannel.id
            reservedchannel_record.whitelist_system_guild = guild.id
            reservedchannel_record.whitelist_system_guild_name = guild
            reservedchannel_record.save()


            info(f"Whitelist Module Log: Owner created Whitelistsystem for Guild: {guild}")


        elif bot_owner != author:
            warn("Whitelist Module Log: Warning, non-owner tried to create Whitelistsystem!")


##################################################################################################
## Funktionen Blueberrys
##################################################################################################

    @commands.Cog.listener()
    async def on_message(self, message):

        if not message.author.bot:

            guild = message.guild

            reserved_chan_query = Whitelistconfig.select().where(Whitelistconfig.whitelist_system_guild == guild.id)
            for item in reserved_chan_query:
                reserved_chan_id = item.whitelist_application_channel

                reserved_chan = self.bot.get_channel(reserved_chan_id)
                whitelistBearbeitungChan = self.bot.get_channel(WHITELIST_EDIT_CHANNEL)


                if "steamcommunity.com" in message.content and message.channel == reserved_chan:

                    info("Whitelist Module Log: Searching for SteamID")

                    user = message.author
                    url = message.content
                    steamid = steam.steamid.from_url(url, http_timeout=10)


                    if steamid == None:

                            warn("Whitelist Module Log: No SteamID found.")

                            await Message.delete(message)
                            embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                            embed.add_field(name=":flag_de:", value="Es konnte kein Steamprofil gefunden werden, bitte prüfe deine Eingabe.", inline=False)
                            embed.add_field(name=":flag_gb:", value="Couldn't find matching Steamprofile, please check your request.", inline=False)
                            await user.send(embed=embed)

                    if steamid != None:
                        
                        info("Whitelist Module Log: SteamID found.")

                        try:
                                ##############################################################################################################################################
                                # ABFRAGE BATTLEMETRICS API 
                                ##############################################################################################################################################
                                info("Whitelist Module Log: Battlemetrics request started.")

                                playerMatchEndpoint = "https://api.battlemetrics.com/players/match"
                                                
                                payloadMatch = json.dumps({"data": [{"type": "identifier", "attributes": {"type": "steamID","identifier": str(steamid)}}]})
                                headers = {
                                    'Content-Type': 'application/json',
                                    'Authorization': f"Bearer {BM_TOKEN}",
                                    'Cookie': '__cfduid=dc8ed5868191043c1a3008ae11c0eaf691612013280'
                                }
                                responseMatch = requests.post(playerMatchEndpoint, headers=headers, data=payloadMatch).json()

                                bmPlayerId = responseMatch["data"][0]["relationships"]["player"]["data"]["id"]
        
                                playerTimePlayedEndpoint = f"https://api.battlemetrics.com/players/{bmPlayerId}/servers/{BM_SERVER_ID_1}"
        
                                responseTimePlayed = requests.get(playerTimePlayedEndpoint, headers=headers).json()
        
                                playerTimePlayed = responseTimePlayed["data"]["attributes"]["timePlayed"]
                                playerTimePlayedCalc = round(playerTimePlayed/60)
                                playerTimePlayedPrint = round(playerTimePlayedCalc/60)

                                info("Whitelist Module Log: Battlemetrics request finished")
        
                                #############################################################################################################################################
                                # ABFRAGE STEAM API / Name & Avatar
                                #############################################################################################################################################
                                
                                info("Whitelist Module Log: Steam API request starting.")
                                
                                url = f"https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/?key={STEAM_TOKEN}&steamids={steamid}"
                                payload={}
                                headers = {}
                                responseSteam = requests.request("GET", url, headers=headers, data=payload)                     
                                steamName = responseSteam.json()["response"]["players"][0]["personaname"]
                                steamAvatar = responseSteam.json()["response"]["players"][0]["avatarfull"]

                                info("Whitelist Module Log: Steam API request finished.")
        
                                ###############################################################################################################################################
                                # VAC Ban Details
                                ###############################################################################################################################################
                                
                                info("Whitelist Module Log: VAC Ban request started.")
                                
                                url = f"https://api.steampowered.com/ISteamUser/GetPlayerBans/v1/?key={STEAM_TOKEN}&steamids={steamid}"
                                payload={}
                                headers = {}
                                responseSteam2 = requests.request("GET", url, headers=headers, data=payload)   
                                vacBans = responseSteam2.json()["players"][0]["NumberOfVACBans"]
                                daysSinceLastBan = responseSteam2.json()["players"][0]["DaysSinceLastBan"]
                                
                                info("Whitelist Module Log: VAC Ban request finished.")
        
                                ###############################################################################################################################################
        
        
                                embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                                embed.add_field(name=":flag_de:", value="Dein Whitelisteantrag ist eingegangen und wird in Kürze von uns bearbeitet!", inline=False)
                                embed.add_field(name=":flag_gb:", value="Your whitelist request has been received and will be processed soon!", inline=False)
                                await user.send(embed=embed)
        
        
                                ###############################################################################################################################################
        
                                embed = discord.Embed(title="Neuer Whitelist Antrag", description="Neuer Whitelistantrag eingegangen.\n", color=0xffd017, inline=False)
                                embed.add_field(name="Antragsteller:", value="```{}```".format(user), inline=True)
                                embed.add_field(name="Steamname:", value="```{}```".format(steamName), inline=True)
                                embed.add_field(name="SteamID:", value="```{}```".format(steamid), inline=False)
                                embed.add_field(name="VAC-Bans:", value="```{}```".format(vacBans), inline=True)
                                embed.add_field(name="Tage seit letztem VAC-Ban:", value="```{}```".format(daysSinceLastBan), inline=True)
                                embed.add_field(name="Spielstunden auf Squadified:", value="```{}```".format(playerTimePlayedPrint), inline=False)
                                embed.add_field(name="Battlemetrics Profil:", value="https://www.battlemetrics.com/rcon/players/{}".format(bmPlayerId), inline=False)
                                embed.set_footer(text="Bei Reaktion mit ✅ erfolgt ein automatischer Eintrag in der Admins.cfg!")
                                embed.set_thumbnail(url=steamAvatar)
        
                                bearbeitungsMsg = await whitelistBearbeitungChan.send(embed=embed)
                                bearbeitungsMsgId = bearbeitungsMsg.id
                                await bearbeitungsMsg.add_reaction("✅")
                                await bearbeitungsMsg.add_reaction("❌")

                                info("Whitelist Module Log: Applicationmessage send to editchannel.")

                                ################################################################################################################################################
                                # EINTRÄGE IN DIE DATENBANK
                                ################################################################################################################################################

                                info("Whitelist Module Log: Starting DB insertion.")

                                application_record = User()
                                application_record.discord_name = user
                                application_record.steam_name = steamName
                                application_record.vac_bans = vacBans
                                application_record.days_since_vac_ban = daysSinceLastBan
                                application_record.steam_id = steamid
                                application_record.discord_user_id = user.id
                                application_record.play_time = playerTimePlayedPrint
                                application_record.application_message = bearbeitungsMsgId
                                application_record.battlemetrics_profile = f"https://www.battlemetrics.com/rcon/players/{bmPlayerId}"
                                application_record.application_status = "Offen"
                                application_record.save()                       
                                
                                info("Whitelist Module Log: DB insertion finished.")

                                await Message.delete(message)
        
                        except (requests.exceptions.ConnectionError, json.decoder.JSONDecodeError):

                            error("Whitelist Module Log: API or JSON Error")
        
                            embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                            embed.add_field(name=":flag_de:", value="Bei deiner Abfrage ist ein Fehler aufgetreten, bitte versuche es später erneut.", inline=False)
                            embed.add_field(name=":flag_gb:", value="An error has occurred, please try again later.", inline=False)
                            await user.send(embed=embed)
        
        
                if "steamcommunity.com" not in message.content and message.channel == reserved_chan:
                    user = message.author
                    await Message.delete(message)
        
                    embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                    embed.add_field(name=":flag_de:", value="Es konnte kein Steamprofil gefunden werden, bitte prüfe deine Eingabe.", inline=False)
                    embed.add_field(name=":flag_gb:", value="Couldn't find matching Steamprofile, please check your request.", inline=False)
                    await user.send(embed=embed)


#############################################################################################################################################
# Whitelist bearbeitung
#############################################################################################################################################


    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):  

        member = (payload.member)
        

        if payload.channel_id == WHITELIST_EDIT_CHANNEL and not member.bot:
            if payload.emoji.name == "✅":

                info("Whitelist Module Log: Whitelistrequest approved. Continue with DB check.")
                editMsgQuery = User.get(User.application_message == payload.message_id)
                if editMsgQuery is None:
                    warn("Whitelist Module Log: No DB entry found.")
                    return
                    

                reservedId = editMsgQuery.application_message
                steamId = editMsgQuery.steam_id
                userid = editMsgQuery.discord_user_id
                steamName = editMsgQuery.steam_name
                user = await self.bot.fetch_user(userid) 


                if payload.message_id == reservedId:
                        
                    with open(adminscfg, "a") as whitelist:
                        whitelist.write(f"\nAdmin={steamId}:Res //{steamName} / {user}")  

                    editMsgQuery.application_status = "Angenommen"
                    editMsgQuery.save()
                    info("Whitelist Module Log: Whitelist and DB entrys done.")


                    embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                    embed.add_field(name=":flag_de:", value="Herzlichen Glückwunsch, dein Whitelistantrag wurde angenommen! Bitte beachte: Aus technischen Gründen kann es bis zu 24 Stunden ab dieser Nachricht dauern bis der Eintrag aktiv wird.", inline=False)
                    embed.add_field(name=":flag_gb:", value="Congratulations, your whitelist application has been accepted!\nPlease note: For technical reasons, it may take up to 24 hours from this message for the entry to become active.", inline=False)
                    await user.send(embed=embed)
                    info("Whitelist Module Log: Usermessage has been send.")                       



                

##########################################################################################################################################


            if payload.emoji.name == "❌" and not member.bot:
                
                info("Whitelist Module Log: Whitelistrequest declined. Continue with DB check.")
                editMsgQuery = User.get(User.application_message == payload.message_id)
                if editMsgQuery is None:
                    warn("Whitelist Module Log: No DB entry found.")
                    return

                reservedId = editMsgQuery.application_message
                steamId = editMsgQuery.steam_id
                userid = editMsgQuery.discord_user_id
                steamName = editMsgQuery.steam_name
                playTime = editMsgQuery.play_time
                user = await self.bot.fetch_user(userid) 


                if payload.message_id == reservedId:

                        
                    embed=discord.Embed(title="SQUADIFIED RESERVED-SLOT SYSTEM")
                    embed.add_field(name=":flag_de:", value="Leider wurde dein reserved-slot Antrag abgelehnt, Grund hierfür könnte die Nichterfüllung einer unserer Kriterien sein, welche du in #reserved-slot nachlesen kannst.\r"
                                                                "Wenn du Fragen hast wende dich einfach über den #create-ticket Kanal an uns!\r"
                                                                "Deine Spielstunden auf unserem Server: ```{}```".format(playTime), inline=False)
                    embed.add_field(name=":flag_gb:", value="We are sorry, but your reserved-slot application has been denied, one reason could be not matching one of our requirements listed in #reserved-slot.\r"
                                                                "If you got any questions feel free to contact us via the #create-ticket channel!\r"
                                                                "Your playtime on our server: ```{}```".format(playTime), inline=False)
                    await user.send(embed=embed)
                        

                    editMsgQuery.application_status = "Abgelehnt"
                    editMsgQuery.save()
                    info("Whitelist Module Log: DB update done and Usermessage send.")


                    


def setup(bot):
    bot.add_cog(Whitelist(bot))