from peewee import *
from settings import db

class Whitelistrequest(Model):
    discord_name = CharField()
    steam_name = CharField(null=True)
    vac_bans = IntegerField()
    days_since_vac_ban = IntegerField()
    steam_id = IntegerField()
    discord_user_id = IntegerField()
    play_time = IntegerField()
    application_message = IntegerField()
    battlemetrics_profile = CharField()
    application_status = CharField()
    
    class Meta:
        database = db 