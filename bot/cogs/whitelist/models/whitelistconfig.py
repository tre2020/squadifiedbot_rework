from peewee import *
from settings import db

class Whitelistconfig(Model):
    whitelist_application_channel = IntegerField()
    whitelist_system_guild = IntegerField()
    whitelist_system_guild_name = CharField()

    class Meta:
        database = db 