import discord
from discord.ext import commands
from settings import *
from utils import *


class Cleaner(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        log("Cleaner Module Log: Module loaded.")

######################## WORK IN PROGRESS ################################

    def is_not_pinned(msg):
        return not msg.pinned


    @commands.command()
    async def clear(self, ctx):
        log("Cleaner Module Log: CLEAR COMMAND HAS BEEN SEEN")
        if ctx.author.permissions_in(ctx.channel).manage_messages:
            args = ctx.message.split(" ")                               # CTX OBJECT HAS NO ATTRIBUTE SPLIT
            if len(args) == 1:
                if not args[0].isdigit():
                    await ctx.send("**Fehler**: Bitte Zahl eingeben!")
                else:
                    count = int(args[0]) + 1
                    deleted = await ctx.message.channel.purge(limit=count, check=is_not_pinned) # WHY IS is_not_pinned NOT RECOGNIZED? 
                    print("**{}** Nachrichten wurden gelöscht.".format(len(deleted)-1))

    @clear.error
    async def clear_error(ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("**Fehler**: Bitte Menge der zu löschenden Nachrichten eingeben!")




def setup(bot):
    bot.add_cog(Cleaner(bot))