import json
import discord
import os
import requests
import datetime
import asyncio
from discord import Message
from discord.ext import commands
from discord.ext import tasks
from settings import *
from utils import *


class Botstatus(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        info("Botstatus Module Log: Module loaded.")
        self.botStatus_task.start()
        info("Botstatus Module Log: Task started.")

    @tasks.loop(seconds=60.0)
    async def botStatus_task(self):

        info("Botstatus Module Log: Request started.")
        url = f'https://api.battlemetrics.com/servers/{BM_SERVER_ID_1}'
        headers = {'Accept': 'application/json'}
        try:
            response = requests.get(url, headers=headers).json()

            players = response["data"]["attributes"]["players"]
            maxplayers = response["data"]["attributes"]["maxPlayers"]
            servername = response["data"]["attributes"]["name"]
            currentmap = response["data"]["attributes"]["details"]["map"]

            await self.bot.change_presence(activity=discord.Game(f"({players}/{maxplayers}) {currentmap}"), status=discord.Status.online)
            # await asyncio.sleep(5)
            info("Botstatus Module Log: Request finished.")

        except (KeyError, requests.exceptions.ConnectionError, json.decoder.JSONDecodeError):
            error("Botstatus Module Log: API or JSON Error")

    @botStatus_task.after_loop
    async def after_botStatus_task():
        info("Botstatus Module Log: Done with botStatus_task")

    @botStatus_task.before_loop
    async def before_botStatus_task(self):
        info("Botstatus Module Log: Waiting to start botStatus_task")
        await self.bot.wait_until_ready()


def setup(bot):
    bot.add_cog(Botstatus(bot))
