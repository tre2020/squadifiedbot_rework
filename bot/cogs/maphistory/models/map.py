import datetime

from peewee import *
from settings import db

class Map(Model):
    name = CharField()
    player_count = IntegerField()
    time_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db 