from peewee import *
from settings import db

class Mapmessages(Model):
    mapHistoryMessageBuffer = IntegerField()

    class Meta:
        database = db 