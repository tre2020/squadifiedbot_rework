import json
import discord
import os
import requests
import datetime
from discord import Message
from discord.ext import commands
from discord.ext import tasks
from peewee import *
from requests.api import head
from settings import db, MOD_CHAT_ID, BM_SERVER_ID_1
from cogs.maphistory.models.map import Map
from cogs.maphistory.models.mapmessages import Mapmessages
from utils import *



class Maphistory(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        info("MapHistory Module Log: Module loaded.")
        self.mapLog_task.start()
        info("MapHistory Module Log: Task started.")



    @tasks.loop(seconds=120.0)
    async def mapLog_task(self):
        url = f'https://api.battlemetrics.com/servers/{BM_SERVER_ID_1}'
        headers = {'Accept': 'application/json'}



        try:
            info("MapHistory Module Log: Request started.")
            response = requests.get(url, headers=headers).json()
            players = response["data"]["attributes"]["players"]
            currentmap = response["data"]["attributes"]["details"]["map"]

            getLastMap = Map.select(Map.name, fn.MAX(Map.id))
            for n in getLastMap:
                lastMap = n.name


            if lastMap != currentmap:
                info("MapHistory Module Log: Current & Lastmap not matching. Creating new entry in Maphistory DB.")
                map_record = Map()
                map_record.name = currentmap
                map_record.player_count = players
                map_record.save()

            if lastMap == currentmap:
                info("MapHistory Module Log: Current & Lastmap matching. Skipping.")


        except (KeyError, requests.exceptions.ConnectionError, json.decoder.JSONDecodeError):
            error("MapHistory Module Log: Battlemetrics API Request or Decoder Error.")





    @commands.command()
    async def lastmaps(self, ctx):

        modChat = self.bot.get_channel(MOD_CHAT_ID)
        oldMessageQuery = Mapmessages.select(Mapmessages.mapHistoryMessageBuffer, fn.MAX(Mapmessages.id))

        for id in oldMessageQuery:
            oldMessage = id.mapHistoryMessageBuffer

        if ctx.channel.id == modChat.id:

            if oldMessage is None:

                # Deleting Users Commandmessage
                await Message.delete(ctx.message)
                info("MapHistory Module Log: Usercommand was deleted.")
                

                # Creating Embed Message
                embed = discord.Embed(title="SQUADIFIED MAP HISTORY",
                                    description="Beginnend mit der aktuellen Map", color=0xffd017)

                lastMapsNameQuery = Map.select().order_by(Map.time_at.desc()).limit(5)
                for n in lastMapsNameQuery:
                    time = n.time_at
                    embed.add_field(name=n.name, value=time.strftime("%b %d / %H:%M"), inline=False)
                 
                buffermessage = await modChat.send(embed=embed)
                buffermessageId = buffermessage.id
                info("MapHistory Module Log: New lastmaps message has been send.")

                mapmessage_record = Mapmessages()
                mapmessage_record.mapHistoryMessageBuffer = buffermessageId
                mapmessage_record.save()
                info("MapHistory Module Log: New lastmaps message id was buffered in DB.")


            if oldMessage is not None:
                oldMessage = await ctx.fetch_message(oldMessage)
                await Message.delete(oldMessage)
                info("MapHistory Module Log: Old lastmaps message was deleted.")
                await Message.delete(ctx.message)     
                info("MapHistory Module Log: Usercommand was deleted.")

                embed = discord.Embed(title="SQUADIFIED MAP HISTORY",
                                    description="Beginnend mit der aktuellen Map", color=0xffd017)
                                    

                lastMapsNameQuery = Map.select().order_by(Map.time_at.desc()).limit(5)
                for n in lastMapsNameQuery:
                    time = n.time_at
                    embed.add_field(name=n.name, value=time.strftime("%b %d / %H:%M"), inline=False)
            
                updateBuffermessage = await modChat.send(embed=embed)
                info("MapHistory Module Log: New lastmaps message has been send.")
                updateBuffermessageId = updateBuffermessage.id
                mapmessage_renew = Mapmessages.get(Mapmessages.id == 1)
                mapmessage_renew.mapHistoryMessageBuffer = updateBuffermessageId
                mapmessage_renew.save()
                info("MapHistory Module Log: New lastmaps message id was buffered in DB.")




def setup(bot):
    bot.add_cog(Maphistory(bot))
